package main

import (
	"bufio"
	"log"
	"os"
)

func main() {
	in, err := readInput()
	if err != nil {
		log.Fatalln("read input:", err)
	}

	p1(in)
	p2(in)
}

func readInput() ([]string, error) {
	f, err := os.Open("input/02")
	if err != nil {
		return nil, err
	}
	defer f.Close()

	var lines []string
	sc := bufio.NewScanner(f)
	for sc.Scan() {
		lines = append(lines, sc.Text())
	}

	return lines, nil
}

func p1(ids []string) {
	var (
		twos   = make(map[string]struct{})
		threes = make(map[string]struct{})
	)
	for _, v := range ids {
		letters := make(map[rune]int)
		for _, c := range v {
			letters[c]++
		}

		for _, n := range letters {
			if _, ok := twos[v]; n == 2 && !ok {
				twos[v] = struct{}{}
			}
			if _, ok := threes[v]; n == 3 && !ok {
				threes[v] = struct{}{}
			}
		}
	}

	log.Println("Checksum:", len(twos)*len(threes))
}

func p2(ids []string) {
	for i := 0; i < len(ids); i++ {
		a := ids[i]
		for j := i + 1; j < len(ids); j++ {
			b := ids[j]

			if len(a) != len(b) {
				continue
			}

			var chdiff, idx int
			for n := 0; n < len(a); n++ {
				if b[n] != a[n] {
					chdiff++
					idx = n
				}
			}

			if chdiff == 1 {
				log.Printf("Common letters: %s%s", a[:idx], a[idx+1:])
				return
			}
		}
	}
}
