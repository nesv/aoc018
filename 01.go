package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func main() {
	in, err := input()
	if err != nil {
		log.Fatalln("read input:", err)
	}

	p1(in)
	p2(in)
}

func input() ([]int, error) {
	f, err := os.Open("input/01")
	if err != nil {
		return nil, err
	}
	defer f.Close()

	var v []int
	sc := bufio.NewScanner(f)
	for sc.Scan() {
		n, err := strconv.Atoi(sc.Text())
		if err != nil {
			return nil, fmt.Errorf("atoi %q", sc.Text())
		}
		v = append(v, n)
	}

	return v, nil
}

func p1(n []int) {
	var hz int
	for _, v := range n {
		hz += v
	}
	log.Println("Frequecy:", hz)
}

func p2(n []int) {
	hzs := map[int]struct{}{0: struct{}{}}
	var hz int
	for {
		for _, v := range n {
			hz += v
			if _, ok := hzs[hz]; ok {
				log.Println("Frequency:", hz)
				return
			}
			hzs[hz] = struct{}{}
		}
	}
}
